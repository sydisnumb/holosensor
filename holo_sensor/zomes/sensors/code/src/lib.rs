#![feature(proc_macro_hygiene)]

use hdk::prelude::*;
use hdk_proc_macros::zome;
use hdk::Dispatch;
use holochain_wasm_utils::api_serialization::get_entry::{
    EntryHistory
};
use holochain_wasm_utils::api_serialization::UpdateEntryArgs;

// see https://developer.holochain.org/api/0.0.51-alpha1/hdk/ for info on using the hdk library

// This is a sample zome that defines an entry type "Sensor" that can be committed to the
// agent's chain via the exposed function create_sensor

#[derive(Serialize, Deserialize, Debug, DefaultJson, Clone)]
pub struct Sensor {
    name: String,
    sensor_type: String,
    quanto: i32,
    tag_loc: String,
    state: bool,
    lat: f64,
    long: f64
}

#[derive(Serialize, Deserialize, Debug, DefaultJson, Clone)]
struct OutputSensor {
    value: f32,
    timestamp: String
}

//Utils struct to return data outputSensor in some functions
#[derive(Serialize, Deserialize, Debug, DefaultJson)]
pub struct GetOutputsResponse {
    name: String,
    list: Vec<OutputSensor>
}

//Utils struct to emit signal for sensors
#[derive(Debug, Serialize, Deserialize, DefaultJson)]
pub struct SignalPayload{
    sensor: Sensor,
    action: String,
    addr: Address
}

#[zome]
mod sensor_zome {

    #[init]
    fn init() {
        Ok(())
    }

    #[validate_agent]
    pub fn validate_agent(validation_data: EntryValidationData<AgentId>) {
        Ok(())
    }

    // Definition of entry sensor, with the validation and linking to outputSensor
    #[entry_def]
    fn sensor_def() -> ValidatingEntryType {
        entry!(
            name: "sensor",
            description: "A sensor is a tool for analyzing the properties of the surrounding environment",
            sharing: Sharing::Public,
            validation_package: || {
                hdk::ValidationPackageDefinition::Entry
            },
            validation: | _validation_data: hdk::EntryValidationData<Sensor>| {
                match _validation_data{
                    hdk::EntryValidationData::Create{entry, .. }=>{
                        if  (entry.sensor_type != "Temperatura" &&
                            entry.sensor_type != "Pressione" && 
                            entry.sensor_type != "Umidità") ||
                            (entry.quanto != 5 &&
                            entry.quanto != 10 &&
                            entry.quanto != 15 &&
                            entry.quanto != 60) {
                            Err("Entry with wrong data".into())
                        }else {Ok(())}
                    },
                    _ => { Ok(()) }
                }
            },
            // link togheter sensor and OutputSensor
            links: [
                to!(
                    "outputSensor",
                    link_type: "output",
                    validation_package: || hdk::ValidationPackageDefinition::Entry,
                    validation: |_validation_data: hdk::LinkValidationData| {
                        Ok(())
                    }
                )
            ]
        )
    }

     // definition of the entry outputSensor 
    #[entry_def]
    fn output_def() -> ValidatingEntryType {  
        entry!(
            name: "outputSensor",
            description: "OutputSensor is the data received by the sensor in a precise instant",
            sharing: Sharing::Public,
            validation_package: || hdk::ValidationPackageDefinition::Entry,
            validation: |_validation_data: hdk::EntryValidationData<OutputSensor>| {
                Ok(())
            }
        )

    }

    // Create operation for sensor entry and emit signal to sensors manager
    #[zome_fn("hc_public")]
    pub fn create_sensor(sensor: Sensor, user_addr: Address, username: String) -> ZomeApiResult<Address> {

        let sens = sensor.clone();
        let entry = Entry::App("sensor".into(), sens.into());
        let sensor_addr = hdk::commit_entry(&entry)?;

        hdk::link_entries(&user_addr, &sensor_addr, "link_sensor", &username)?;

        hdk::emit_signal("message_received", JsonString::from_json(&format!("{{\"action\": \"{}\", \"type\": \"{}\", \"state\": \"{}\", \"addr\": \"{}\", \"quanto\": {}}}", "start", sensor.sensor_type, sensor.state, sensor_addr, sensor.quanto)))?;

        Ok(sensor_addr)
    }

    // Update operation for sensor entry and emit signal to sensors manager
    #[zome_fn("hc_public")]
    pub fn update_sensor(sensor: Sensor, address: Address) -> ZomeApiResult<Address> {

        let sens = sensor.clone();
        let entry = Entry::App("sensor".into(), sens.into());
        
   
        let addr = Dispatch::UpdateEntry.with_input(UpdateEntryArgs {
            new_entry: entry,
            address: address.clone(),
        });

        hdk::emit_signal("message_received", JsonString::from_json(&format!("{{\"action\": \"{}\", \"type\": \"{}\", \"state\": \"{}\", \"addr\": \"{}\", \"quanto\": {}}}", "start", sensor.sensor_type, sensor.state, address, sensor.quanto)))?;

        addr
    }

    // Remove operation for sensor entry and emit signal to sensors manager
    #[zome_fn("hc_public")]
    pub fn remove_sensor(address: Address) -> ZomeApiResult<Address> {
        let addr = Dispatch::RemoveEntry.with_input(address.to_owned());

        hdk::emit_signal("message_received", JsonString::from_json(&format!("{{\"action\": \"{}\", \"addr\": \"{}\"}}", "stop",  address)))?;

        addr
    }

    // Return the whole history (update, delete) for an sensor entry
    #[zome_fn("hc_public")]
    pub fn get_sensor_history(address: Address) -> ZomeApiResult<Option<EntryHistory>> {
        hdk::api::get_entry_history(&address)
    }

    // Return an sensor entry by address
    #[zome_fn("hc_public")]
    fn get_sensor(address: Address) -> ZomeApiResult<Sensor> {
        let sensor = hdk::utils::get_as_type::<Sensor>(address.clone())?;
        
        Ok(sensor)
    }

    // Return all semsors entries
    #[zome_fn("hc_public")]
    fn get_sensors() -> ZomeApiResult<QueryResult> {
        let vec_address = hdk::query_result("sensor".into(), QueryArgsOptions{ start: 0, limit: 0, headers: true, entries: true})?;
    
        Ok(vec_address)
    }

    // Add a new output to a sensor entry
    #[zome_fn("hc_public")]
    fn add_output_sensor(output: OutputSensor, sensor_addr: Address) -> ZomeApiResult<Address> {
        // define the entry
        let output_entry = Entry::App(
            "outputSensor".into(),
            output.into()
        );
    
        let output_addr = hdk::commit_entry(&output_entry)?; // commit the output
        hdk::link_entries(&sensor_addr, &output_addr, "output","")?; // if successful, link to sensor address

        Ok(output_addr)
    }
    
    // Return all outputs of a sensor
    #[zome_fn("hc_public")]
    fn get_all_output_sensor(sensor_addr: Address) -> ZomeApiResult<GetOutputsResponse> {
    
        // load the sensor entry. Early return error if it cannot load or is wrong type
        let sensor = hdk::utils::get_as_type::<Sensor>(sensor_addr.clone())?;
    
        // try and load the sensor outputs, filter out errors and collect in a vector
        let list_outputs = hdk::get_links(&sensor_addr, LinkMatch::Exactly("output"), LinkMatch::Any)?.addresses()
            .iter()
            .map(|output_address| {
                hdk::utils::get_as_type::<OutputSensor>(output_address.to_owned())
            })
            .filter_map(Result::ok)
            .collect::<Vec<OutputSensor>>();
    
        // if this was successful then return the outputs
        Ok(GetOutputsResponse{
            name: sensor.name,
            list: list_outputs
        })
    }
}
