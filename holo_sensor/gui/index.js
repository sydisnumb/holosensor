var holochain_connection = holochainclient.connect();

function login() {
    holochain_connection.then(({ callZome, close }) => {

        var username = $('#username_input').val();
        var psw = $('#password_input').val();

        //const hash = digestMessage('SHA-256', psw).toString();

        callZome(
            'test-instance',
            'users',
            'get_user_by_id',
        )({ id: username, }).then(result => {
            var user = JSON.parse(result);

            if (user.Ok[0] != undefined) {
                var entry = JSON.parse(user.Ok[0].Ok.result.Single.entry.App[1]);
                if (psw == entry.password) {
                    console.log("psw right");
                    sessionStorage.setItem('address', user.Ok[0].Ok.result.Single.meta.address)
                    sessionStorage.setItem('username', username)
                    window.location.replace("./sensors/dashboard.html");
                } else {
                    alert("Incorrect username or password");
                }

            } else {
                alert("Incorrect username or password!");
            }
        });
    });
}

// HASH NON UTILIZZATA A CAUSA DELLA GESTIONE ASINCRONA CHE DA PROBLEMI
// async function digestMessage(message) {
//     const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
//     const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
//     const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
//     const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
//     console.log("dentro== "+hashHex);
//     return hashHex;
//   }