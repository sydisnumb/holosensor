var holochain_connection = holochainclient.connect();

function show_output(result, id) {
  var el = document.getElementById(id);
  var output = JSON.parse(result);
  if (output.Ok) {
    console.log(output.Ok);
    el.textContent = output.Ok;
  } else {
    alert(output.Err.Internal);
  }
}
function create_sensor() {
  const nome = document.getElementById('nome').value;
  const tipo = document.getElementById('tipo').value;
  const quanto = document.getElementById('quanto').value;
  const tag_loc = document.getElementById('locazione').value;
  //const stato = document.getElementById('stato').value;
  const lat = document.getElementById('latitudine').value;
  const long = document.getElementById('longitudine').value;
  var address = document.getElementById('address_user').value.trim();
  var username = document.getElementById('username_user').value.trim();

  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'create_sensor')({
      sensor: {
        name: nome,
        sensor_type: tipo,
        quanto: Number(quanto),
        tag_loc: tag_loc,
        state: false,
        lat: Number(lat),
        long: Number(long)
      },
      user_addr: address,
      username: username
    }).then(result => {
      show_output(result, "address_output");
      console.log(result);
    });
  });
}

function update_sensor() {
  const nome = document.getElementById('nome').value;
  const tipo = document.getElementById('tipo').value;
  const quanto = document.getElementById('quanto').value;
  const tag_loc = document.getElementById('locazione').value;
  //const stato = document.getElementById('stato').value;
  const lat = document.getElementById('latitudine').value;
  const long = document.getElementById('longitudine').value;
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'update_sensor')({
      sensor: {
        name: nome,
        sensor_type: tipo,
        quanto: Number(quanto),
        tag_loc: tag_loc,
        state: false,
        lat: Number(lat),
        long: Number(long)
      },
      address: address
    }).then(result => {
      show_output(result, "address_output");
      console.log(result);
    });
  });
}

function remove_sensor() {
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'remove_sensor')({
      address: address
    }).then(result => {
      show_output(result, "address_output");
      console.log(result);
    });
  });
}



function get_sensor() {
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'get_sensor')({
      address: address,
    }).then(result => {
      show_sensor(result);
      console.log(result);
    })
  });
}

function get_sensors() {
  holochain_connection.then(({ callZome, close }) => {
    console.log("sto dentro a get")
    callZome('test-instance', 'sensors', 'get_sensors')({ args: {} }).then(result => show_sensors(result, 'list_sensors'));
  });
}

function get_sensor_history() {
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'get_sensor_history')({
      address: address,
    }).then(result => show_history(result, 'sensor_history'));
  });
}

function show_history(result, id) {
  document.getElementById('sensor_history').textContent = "";
  var el = document.getElementById(id);
  var output = JSON.parse(result);
  //el.textContent = result
  for (i in output.Ok.items) {

    console.log(output.Ok);
    console.log(output.Ok)
    console.log(output.Ok.items[i].meta.crud_status)
    console.log(output.Ok.items[i].meta.entry_type.App)
    console.log(output.Ok.items[i].meta.address)


    $("#sensor_history").append("Crud status: " + output.Ok.items[i].meta.crud_status + "\n");
    $("#sensor_history").append("entry_type: " + output.Ok.items[i].meta.entry_type.App + "\n");
    $("#sensor_history").append("Address: " + output.Ok.items[i].meta.address + "\n");
    if (output.Ok.items[i].entry.Deletion) {
      $("#sensor_history").append("deleted_entry_address: " + output.Ok.items[i].entry.Deletion.deleted_entry_address + "\n");
    } else {
      var sensor = JSON.parse(output.Ok.items[i].entry.App[1]);
      console.log(output.Ok.items[i].entry.App[1]);
      //$("#sensor_history").append("Sensore " + (parseInt(i) + 1) + "\n");
      //$("#sensor_history").append("Address " + output.Ok.HeadersWithEntries[i][0].entry_address + "\n");
      $("#sensor_history").append("Nome: " + sensor.name + "\n");
      $("#sensor_history").append("Tipologia: " + sensor.sensor_type + "\n");
      $("#sensor_history").append("Quanto: " + sensor.quanto + "\n");
      $("#sensor_history").append("Tag_loc: " + sensor.tag_loc + "\n");
      $("#sensor_history").append("State: " + sensor.state + "\n");
      $("#sensor_history").append("Lat: " + sensor.lat + "\n");
      $("#sensor_history").append("long: " + sensor.long + "\n\n");
    }
  }

}

function show_sensors(result, id) {
  document.getElementById('list_sensors').textContent = "";
  var output = JSON.parse(result);

  if (output.Ok) {
    for (i in output.Ok.HeadersWithEntries) {
      var sensor = JSON.parse(output.Ok.HeadersWithEntries[i][1].App[1]);
      $("#list_sensors").append("Sensore " + (parseInt(i) + 1) + "\n");
      $("#list_sensors").append("Address " + output.Ok.HeadersWithEntries[i][0].entry_address + "\n");
      $("#list_sensors").append("Nome: " + sensor.name + "\n");
      $("#list_sensors").append("Tipologia: " + sensor.sensor_type + "\n");
      $("#list_sensors").append("Quanto: " + sensor.quanto + "\n");
      $("#list_sensors").append("Tag_loc: " + sensor.tag_loc + "\n");
      $("#list_sensors").append("State: " + sensor.state + "\n");
      $("#list_sensors").append("Lat: " + sensor.lat + "\n");
      $("#list_sensors").append("long: " + sensor.long + "\n\n");
    }
  }
}

function show_sensor(result) {
  var nome = document.getElementById('s_nome');
  var tipo = document.getElementById('s_tipo');
  var quanto = document.getElementById('s_quanto');
  var tag_loc = document.getElementById('s_locazione');
  var stato = document.getElementById('s_stato');
  var lat = document.getElementById('s_latitudine');
  var long = document.getElementById('s_longitudine');
  var output = JSON.parse(result);
  console.log(output)
  if (output.Ok) {

    nome.textContent = output.Ok.name;
    tipo.textContent = output.Ok.sensor_type;
    quanto.textContent = output.Ok.quanto;
    tag_loc.textContent = output.Ok.tag_loc;
    stato.textContent = output.Ok.state;
    lat.textContent = output.Ok.lat;
    long.textContent = output.Ok.long;
  } else {
    alert(output.Err.Internal);
  }
}

function add_output_sensor() {
  const valore = document.getElementById('valore').value;
  const tempo = document.getElementById('time').value;
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'add_output_sensor')({
      output: {   //passare nome argomento
        value: Number(valore),
        timestamp: tempo
      },
      sensor_addr: address
    }).then(result => {
      show_output(result, "address_sensor");
      console.log(result);
    });
  });
}

function get_all_output_sensor() {
  var address = document.getElementById('address_in_output').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'sensors', 'get_all_output_sensor')({
      sensor_addr: address,
    }).then(result => {
      show_all_output_sensor(result);
      console.log(result);
    });
  });
}

function show_all_output_sensor(result) {
  var nome = document.getElementById('nomelista');

  var output = JSON.parse(result);
  console.log(output.Ok.list[0]);
  if (output.Ok) {
    for (i in output.Ok.list) {
      console.log("FDJKSHFK " + output.Ok.list[i].value);
      var outputSensor = output.Ok.list[i];
      $("#lista_outputs").append("Output " + (parseInt(i) + 1) + "\n");
      $("#lista_outputs").append("Value: " + outputSensor.value + "\n");
      $("#lista_outputs").append("Timestamp: " + outputSensor.timestamp + "\n\n");
    }
  } else {
    alert(output.Err.Internal);
  }
}