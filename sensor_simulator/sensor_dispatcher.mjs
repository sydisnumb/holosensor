import { createRequire } from 'module';
import pkg from '@holochain/hc-web-client';

const require = createRequire(import.meta.url);
const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const { connect } = pkg;

var threadMap = new Map();



// ------------ Handler Dispatcher ---------------
console.log("Sensor Dispatcher is running...");
connect({ url: "http://10.0.0.100:50000" }).then(({ callZome, close, onSignal }) => {
    onSignal((msg) => {
        console.log(msg.signal.arguments);
        let obj = JSON.parse(msg.signal.arguments);
        handleReq(obj);
        console.log("Sensor Dispatcher: connection established with HoloSensor!");
    });
})

// -------------------- Utility function ------------------
function handleReq(body) {
    console.log("\nPlaying for " + body.addr + "!");

    var sensore = getSensor(body);
    sensore.postMessage(body);

    if (body.action == "stop") {
        sensore.terminate();
        threadMap.delete(body.addr);
        console.debug("Sensor " + body.addr + " is stopping...");
    }
}

function getSensor(conf) {

    if (threadMap.get(conf.addr)) {
        return threadMap.get(conf.addr);
    }

    var sensore = new Worker("./sensor.mjs", { workerData: conf });
    threadMap.set(conf.addr, sensore);

    return sensore;
}

