console.log(sessionStorage.getItem("address"))

if (sessionStorage.getItem("address") == undefined) {
    window.location.replace("../index.html");
}

var holochain_connection = holochainclient.connect();
var mymap = L.map('mapid').setView([51.505, -0.09], 13);
var marker = L.marker([51.5, -0.09]).addTo(mymap);
var ctx = document.getElementById('myChart').getContext('2d');
var char;


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(mymap);

// Initialization of the page body
function init() {
    holochain_connection.then(({ callZome, close }) => {

        // Come back from the Session Storage
        var addr = sessionStorage.getItem("address");
        var username = sessionStorage.getItem("username");

        var searched_usr = sessionStorage.getItem("searched_usr");
        var searched_addr = sessionStorage.getItem("searched_addr");

        console.log("my user " + username);
        console.log("my addr " + addr);
        console.log("searched user " + searched_usr);
        console.log("searched addr " + searched_addr);

        if (searched_usr != undefined) {
            console.log(searched_usr);

            username = searched_usr;
            addr = searched_addr;
            $(".navbar_footer").removeClass( "bg-dark");
            $(".navbar_footer").css("background-color", "#888B99");
            $("#sensor_usr").text("These are the sensors of " + username);
        }


        console.log("my user " + username);
        console.log("my addr " + addr);
        console.log("searched user " + searched_usr);
        console.log("searched addr " + searched_addr);
        callZome(
            'test-instance',
            'users',
            'get_user_sensor',
        )({ base: addr, username: username }).then(result => {
            var sensors = JSON.parse(result);
            var firstEl = true;

            console.log("sensori " + result)
            // if there are sensors --> Show all
            if (sensors.Ok.length) {
                var flag = true;

                if (searched_usr == username) {
                    $("#addButton").attr("style", "display: none;");
                    $("#deleteButton").attr("style", "display: none;");

                }

                sensors.Ok.forEach(function (obj) {

                    if (obj.Ok.result.Single.entry != undefined) {
                        flag = false;
                        var address = obj.Ok.result.Single.meta.address;
                        var content = JSON.parse(obj.Ok.result.Single.entry.App[1]);

                        var $a = $("<a>");
                        $a.attr('class', 'list-group-item list-group-item-action');
                        $a.attr('id', address);
                        $a.attr('onclick', 'getSensor(this)');
                        $a.attr('data-toggle', 'list');
                        $a.attr('role', 'tab');
                        $a.attr('aria-controls', content.name);
                        $a.text(content.name);


                        if (firstEl) {
                            $a.addClass('active');
                            firstEl = false;
                            getOutputSensor(address);
                            showSensorFeauture(content, address);
                        }
                        $("#listSensori").append($a);
                        $("#dashSensor").attr("style", "visibility: visible;");
                    }
                }

                );
                if (flag) {

                    if (searched_usr == username) {
                        alert("This user hasn't sensors. Try another one!");
                        location.reload();
                    }

                    $("#dashSensor").attr("style", "display: none;");
                    $("#divZeroSensori").attr("style", "visibility: visible");
                }
            } else {

                if (searched_usr == username) {
                    alert("This user hasn't sensors. Try another one!");
                    location.reload();
                }

                $("#dashSensor").attr("style", "display: none;");
                $("#divZeroSensori").attr("style", "visibility: visible");
            }


        });
        sessionStorage.removeItem('searched_addr');
        sessionStorage.removeItem("searched_usr");
    });
}



$("#searchbar").keyup(function (e) {
    if (e.keyCode === 13) {
        search();
    }
});

function search() {

    var searched_usr = $('#searchbar').val();

    holochain_connection.then(({ callZome, close }) => {
        callZome(
            'test-instance',
            'users',
            'get_user_by_id',
        )({ id: searched_usr, }).then(result => {
            console.log(result);
            var user = JSON.parse(result);

            if (user.Ok[0] != undefined) {
                var entry = JSON.parse(user.Ok[0].Ok.result.Single.entry.App[1]);

                sessionStorage.setItem('searched_addr', user.Ok[0].Ok.result.Single.meta.address);
                sessionStorage.setItem("searched_usr", searched_usr);

                location.reload();

            } else {
                alert("Incorrect username!");
            }
        });
    });
}

// Return a single sensor
function getSensor(sensor) {
    holochain_connection.then(({ callZome, close }) => {
        callZome(
            'test-instance',
            'sensors',
            'get_sensor',
        )({ address: sensor.id }).then(result => {
            var obj = JSON.parse(result);
            getOutputSensor(sensor.id);
            showSensorFeauture(obj.Ok, sensor.id);
        });
    });
}

// Remove a sensor
function remove_sensor() {
    var address = $("#addressLabel").text();
    holochain_connection.then(({ callZome, close }) => {
        callZome('test-instance', 'sensors', 'remove_sensor')({
            address: address
        }).then(result => {
            window.location.reload(true);
        });
    });

}

// Show the outputs of the sensor
function getOutputSensor(address) {
    holochain_connection.then(({ callZome, close }) => {
        callZome(
            'test-instance',
            'sensors',
            'get_all_output_sensor',
        )({ sensor_addr: address }).then(result => {
            var obj = JSON.parse(result);
            drawGraph(obj.Ok.list);
        });
    });
}

// Show all the features of a sensor
function showSensorFeauture(sensor, address) {

    if (sensor == undefined) {
        window.location.reload(true);
    }
    console.log(sensor + "----------- " + address)
    $('#nameLabel').text(sensor.name);
    $('#campionamentoLabel').text(sensor.quanto + " min");
    $('#addressLabel').text(address);
    $('#nameLabel').text(sensor.name);
    $('#indirizzoLabel').text(sensor.tag_loc);
    mymap.setView([sensor.lat, sensor.long], 13);
    marker.setLatLng([sensor.lat, sensor.long]);
    marker.bindPopup(sensor.tag_loc).openPopup();

    if (sensor.state) {
        $('#circleStatus').html("&#128154;");
    }
}

function drawGraph(el) {
    if (el.length != 0) {
        $('#myChart').attr("style", "visibility: visible;");

        var values = [];
        var labelsOutput = [];
        for (var i = el.length - 1; i > 0 && i > el.length - 20; i--) {
            values.push(el[i].value);
            labelsOutput.push(el[i].timestamp);
        }

        char = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: labelsOutput,
                datasets: [{
                    label: 'Sample hold',
                    borderColor: 'rgb(255, 99, 132)',
                    data: values
                }]
            },
        });
    }
}

function logout() {
    sessionStorage.clear()
    window.location.replace("../index.html");
}

