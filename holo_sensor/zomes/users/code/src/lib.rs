#![feature(proc_macro_hygiene)]

use hdk::api::get_links_count_with_options;
use hdk::api::update_entry;
use hdk::prelude::*;
use hdk::Dispatch;
use hdk_proc_macros::zome;
use holochain_wasm_utils::api_serialization::get_entry::{EntryHistory};

// see https://developer.holochain.org/api/0.0.51-alpha1/hdk/ for info on using the hdk library

// This is a sample zome that defines an entry type "User" that can be committed to the
// agent's chain via the exposed function create_user

#[derive(Serialize, Deserialize, Debug, DefaultJson, Clone)]
pub struct User {
    name: String,
    surname: String,
    username: String,
    password: String,
}

#[zome]
mod user_zome {

    #[init]
    fn init() {
        Ok(())
    }

    #[validate_agent]
    pub fn validate_agent(validation_data: EntryValidationData<AgentId>) {
        Ok(())
    }

    // Definition of entry user, with the validation and linking to sensors
    #[entry_def]
    fn definition() -> ValidatingEntryType {
        entry!(
            name: "user",
            description: "A user is a real person that can access at the network to analyze the data shared into it",
            sharing: Sharing::Public,
            validation_package: || {
                hdk::ValidationPackageDefinition::Entry
            },
            validation: | _validation_data: hdk::EntryValidationData<User>| {
                match _validation_data{
                    hdk::EntryValidationData::Create{entry,validation_data:_}=>{
                        // anchor useful to find the users in our app when we don't know their adresses
                        let anchor_entry = Entry::App("anchor".into(), "users".into());
                        let anchor_address = hdk::commit_entry(&anchor_entry)?;
                        let all_users =  hdk::api::get_links_and_load(
                            &anchor_address,
                            // Match the link_type exactly has_user.
                            LinkMatch::Exactly("has_user"),
                            // Match exactly the tag.
                            LinkMatch::Exactly(&entry.username),
                        );

                        // If username exists --> OK, otherwise --> error
                        if all_users.unwrap().len() == 0 {
                            Ok(())
                        } else {
                            Err("User already exists".into())
                        }
                    },
                    _ => Ok(()),
                }
            },
            // link togheter user and sensor
            links: [
                to!(
                    "sensor",
                    link_type: "link_sensor",
                    validation_package: || hdk::ValidationPackageDefinition::Entry,
                    validation: |_validation_data: hdk::LinkValidationData| {
                        Ok(())
                    }
                )
            ]
        )
    }

    // definition of the entry anchor that we use to returns all the users
    #[entry_def]
    fn anchor_entry_def() -> ValidatingEntryType {
        entry!(
            name: "anchor",
            description: "Anchor to all the links",
            sharing: Sharing::Public,       // entry saved in DHT and Source Chain
            validation_package: || {
                hdk::ValidationPackageDefinition::Entry
            },
            validation: |_validation_data: hdk::EntryValidationData<String>| {
                Ok(())
            },

            links: [
            to!(
                // Link to the user entry
                "user",
                // This link is a has_user link
                link_type: "has_user",
               validation_package: || {
                   hdk::ValidationPackageDefinition::Entry
               },
               validation: |_validation_data: hdk::LinkValidationData| {
                   Ok(())
               }
            )
            ]
        )
    }

    // Create operation for user entry
    #[zome_fn("hc_public")]
    pub fn create_user(user: User) -> ZomeApiResult<Address> {
        let user2 = user.clone();
        let entry = Entry::App("user".into(), user.into());

        let anchor_entry = Entry::App("anchor".into(), "users".into());
        let anchor_address = hdk::commit_entry(&anchor_entry)?;

        let user_address = hdk::commit_entry(&entry)?;
        // Link the anchor to the user.
        hdk::link_entries(&anchor_address, &user_address, "has_user", &user2.username)?;
        Ok(user_address)
    }

    // Update operation for user entry
    #[zome_fn("hc_public")]
    pub fn update_user(user: User, address: Address) -> ZomeApiResult<Address> {
        let entry = Entry::App("user".into(), user.into());
        update_entry(entry, &address)
    }

    // Remove operation for user entry
    #[zome_fn("hc_public")]
    pub fn remove_user(address: Address) -> ZomeApiResult<Address> {
        Dispatch::RemoveEntry.with_input(address.to_owned())
    }

    // Return the whole history (update, delete) for an user entry
    #[zome_fn("hc_public")]
    pub fn get_user_history(address: Address) -> ZomeApiResult<Option<EntryHistory>> {
        hdk::api::get_entry_history(&address)
    }

    // Return an user entry by address
    #[zome_fn("hc_public")]
    pub fn get_user(address: Address) -> ZomeApiResult<User> {
        hdk::utils::get_as_type(address)
    }

    // Return all user entries by anchor link
    #[zome_fn("hc_public")]
    fn get_users_by_anchor() -> ZomeApiResult<Vec<User>> {
        let anchor_entry = Entry::App("anchor".into(), "users".into());
        let anchor_address = hdk::commit_entry(&anchor_entry)?;

        hdk::utils::get_links_and_load_type(
            &anchor_address,
            LinkMatch::Exactly("has_user"),
            LinkMatch::Any,
        )
    }

    // Return user entry by id
    #[zome_fn("hc_public")]
    fn get_user_by_id(id: String) -> ZomeApiResult<Vec<ZomeApiResult<GetEntryResult>>> {
        let anchor_entry = Entry::App("anchor".into(), "users".into());
        let anchor_address = hdk::commit_entry(&anchor_entry)?;

        hdk::api::get_links_result(
            &anchor_address,
            LinkMatch::Exactly("has_user"),
            LinkMatch::Exactly(&id),
            GetLinksOptions::default(),
            GetEntryOptions::default(),
        )
    }

    // Return the number of the link of an entry
    #[zome_fn("hc_public")]
    pub fn get_links_count(base: Address) -> ZomeApiResult<GetLinksResultCount> {
        get_links_count_with_options(
            &base,
            LinkMatch::Exactly("link_sensor"),
            LinkMatch::Any,
            GetLinksOptions::default(),
        )
    }

    // Return all the sensor entry linked to the user entry
    #[zome_fn("hc_public")]
    pub fn get_user_sensor(base: Address, username: String) -> ZomeApiResult<Vec<ZomeApiResult<GetEntryResult>>> {
        hdk::api::get_links_result(
            &base,
            LinkMatch::Exactly("link_sensor"),
            LinkMatch::Exactly(&username),
            GetLinksOptions::default(),
            GetEntryOptions::default(),
        )
    }
}
