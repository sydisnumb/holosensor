
if (sessionStorage.getItem("address") == undefined) {
    window.location.replace("../index.html");
}

var holochain_connection = holochainclient.connect();
var mymap = L.map('mapid').setView([51.505, -0.09], 13);
var geocodeService = L.esri.Geocoding.geocodeService();
var marker = L.marker([51.5, -0.09]).addTo(mymap);
var latitudine;
var longitudine;

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(mymap);


// Return the position on the map
function onMapClick(e) {
    marker.setLatLng(e.latlng);
    latitudine = e.latlng.lat;
    longitudine = e.latlng.lng;

    geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
        if (error) {
            return;
        }

        $("#sensorLocation").val(result.address.Match_addr);
    });
}

mymap.on('click', onMapClick);


// Create and store a new Sensor
function saveSensor() {

    if ($("#sensorName").val() != "" && $("#sensorLocation").val() != "") {
        holochain_connection.then(({ callZome, close }) => {
            console.log(latitudine);

            var addr = sessionStorage.getItem('address')
            var username = sessionStorage.getItem('username')
            console.log(addr, username, JSON.stringify({ sensor: { name: $("#sensorName").val(), type_sensor: $("#sensorType").val(), quanto: parseInt($("#sensorQuantum").val()), tag_loc: $("#sensorLocation").val(), state: true, lat: latitudine, long: longitudine }, user_addr: addr, username: username }));
            callZome(
                'test-instance',
                'sensors',
                'create_sensor',
            )({ sensor: { name: $("#sensorName").val(), sensor_type: $("#sensorType").val(), quanto: parseInt($("#sensorQuantum").val()), tag_loc: $("#sensorLocation").val(), state: true, lat: latitudine, long: longitudine }, user_addr: addr, username: username }).then(result => {
                console.log(result);

                if (JSON.parse(result).Err == undefined) {
                    $("#alert").attr("style", "display: block");
                    $("#saveButton").attr("style", "display: none");
                    $("#reloadButton").attr("style", "display: block");
                    $("#backButton").attr("style", "display: block");
                    $("#sensorName").attr("disabled", true);
                    $("#sensorQuantum").attr("disabled", true);
                    $("#sensorType").attr("disabled", true);
                    mymap.dragging.disable();
                    mymap.touchZoom.disable();
                    mymap.doubleClickZoom.disable();
                    mymap.scrollWheelZoom.disable();
                    mymap.boxZoom.disable();
                    mymap.off('click');
                } else {
                    alert("Something went wrong. Try again later!");
                }

            });
        });
    } else {
        alert("Enter name sensor and location!");
    }
}

function reload() {
    location.reload(true);
}

function backDashboard() {
    window.location.replace("./dashboard.html");
}

function logout() {
    sessionStorage.clear()
    window.location.replace("../index.html");
}
