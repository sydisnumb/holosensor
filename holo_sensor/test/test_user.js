/// NB: The tryorama config patterns are still not quite stabilized.
/// See the tryorama README [https://github.com/holochain/tryorama]
/// for a potentially more accurate example

const path = require('path');

const {
  Orchestrator,
  Config,
  combine,
  localOnly,
  tapeExecutor,
} = require('@holochain/tryorama');

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.error('got unhandledRejection:', error);
});

const dnaPath = path.join(__dirname, '../dist/holo_sensor.dna.json');

const orchestrator = new Orchestrator({
  middleware: combine(
    // use the tape harness to run the tests, injects the tape API into each scenario
    // as the second argument
    tapeExecutor(require('tape')),

    // specify that all "players" in the test are on the local machine, rather than
    // on remote machines
    localOnly,
  ),
});

const dna = Config.dna(dnaPath, 'holo_sensor');
const config = Config.gen(
  {
    holochain_sensor: dna,
  },
  {
    network: {
      type: 'sim2h',
      sim2h_url: 'ws://localhost:9000',
    },
  },
);
orchestrator.registerScenario('Test Utente', async (s, t) => {
  const {alice, bob} = await s.players({alice: config, bob: config}, true);
  const result = await alice.call('holo_sensor', 'users', 'hello_holo', {});
  t.ok(result.Ok);
  t.deepEqual(result, {Ok: 'Hello Holo'});

  const create_result = await alice.call('holo_sensor', 'users', 'create_utente', {
    utente: {nome: 'Alice',
             cognome: "bella",
             mail: "a.bella@mail.com",
             password: "qwerty"
            },
  });
  t.ok(create_result.Ok);
  const alice_utente_address = create_result.Ok;

  await s.consistency();

  const retrieve_result = await alice.call(
    'holo_sensor',
    'users',
    'get_utente',
    {address: alice_person_address},
  );
  t.ok(retrieve_result.Ok);
  t.deepEqual(retrieve_result, {Ok: {nome: 'Alice',
                                    cognome: "bella",
                                    mail: "a.bella@mail.com",
                                    password: "qwerty"
                                    }});
});

orchestrator.run();