var holochain_connection = holochainclient.connect();

async function digestMessage(message) {
  const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
  console.log("dentro== " + hashHex);
  return hashHex;
}

function show_output(result, id) {
  var el = document.getElementById(id);
  var output = JSON.parse(result);
  if (output.Ok) {
    el.textContent = output.Ok;
  } else {
    alert(output.Err.Internal);
  }
}

function create_user() {
  const name = document.getElementById('name').value;
  const surname = document.getElementById('surname').value;
  const username = document.getElementById('username').value;
  const psw = document.getElementById('psw').value;
  const hash = digestMessage('SHA-256', psw).toString();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'users', 'create_user')({
      user: {
        name: name,
        surname: surname,
        username: username,
        password: psw
      },
    }).then(result => {
      show_output(result, 'address_output');
      console.log(result);
    });
  });
}

function update_user() {
  const name = document.getElementById('name').value;
  const surname = document.getElementById('surname').value;
  const username = document.getElementById('username').value;
  const psw = document.getElementById('psw').value;
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'users', 'update_user')({
      user: {
        name: name,
        surname: surname,
        username: username,
        password: psw
      },
      address: address
    }).then(result => {
      show_output(result, 'address_output');
      console.log(result);
    });
  });
}

function remove_user() {
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'users', 'remove_user')({
      address: address
    }).then(result => {
      show_output(result, 'address_output');
      console.log(result);
    });
  });
}

function get_user() {
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'users', 'get_user')({
      address: address,
    }).then(result => {
      show_user(result);
      console.log(result);
    });
  });
}

function get_user_by_id() {
  var id = document.getElementById('id_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'users', 'get_user_by_id')({
      id: id,
    }).then(result => {
      show_user_by_id(result);
      console.log(result);
    });
  });
}

function get_users() {
  holochain_connection.then(({ callZome, close }) => {
    console.log("sto dentro a get")
    callZome('test-instance', 'users', 'get_users_by_anchor')({ args: {} }).then(result => {
      show_users(result, 'list_users');
      console.log(result);
    });
  });
}

function get_user_sensor() {
  var username = document.getElementById('username_user_sensor').value.trim();
  var address = document.getElementById('address_base').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    console.log("sto dentro a get")
    callZome('test-instance', 'users', 'get_user_sensor')({ base: address, username: username }).then(result => {
      show_sensors(result, 'list_sensors');
      console.log(result);
    });
  });
}

function get_links_count() {
  var address = document.getElementById('address_base').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    console.log("sto dentro a get")
    callZome('test-instance', 'users', 'get_links_count')({
      base: address
    }).then(result => show_count(result, "num_sensors"));
  });

}

function show_count(result, id) {
  var el = document.getElementById(id);
  var output = JSON.parse(result);
  if (output.Ok) {
    el.textContent = output.Ok.count;
  } else {
    alert(output.Err.Internal);
  }
}

function get_user_history() {
  var address = document.getElementById('address_in').value.trim();
  holochain_connection.then(({ callZome, close }) => {
    callZome('test-instance', 'users', 'get_user_history')({
      address: address,
    }).then(result => show_history(result, 'user_history'));
  });
}

function show_history(result, id) {
  document.getElementById('user_history').textContent = "";
  var el = document.getElementById(id);
  var output = JSON.parse(result);
  //el.textContent = result
  for (i in output.Ok.items) {
    console.log(output.Ok);



    console.log(output.Ok)
    console.log(output.Ok.items[i].meta.crud_status)
    console.log(output.Ok.items[i].meta.entry_type.App)
    console.log(output.Ok.items[i].meta.address)


    $("#user_history").append("Crud status: " + output.Ok.items[i].meta.crud_status + "\n");
    $("#user_history").append("entry_type: " + output.Ok.items[i].meta.entry_type.App + "\n");
    $("#user_history").append("Address: " + output.Ok.items[i].meta.address + "\n");
    if (output.Ok.items[i].entry.Deletion) {
      $("#user_history").append("deleted_entry_address: " + output.Ok.items[i].entry.Deletion.deleted_entry_address + "\n");
    } else {
      var user = JSON.parse(output.Ok.items[i].entry.App[1]);
      console.log(user);
      $("#user_history").append("Nome: " + user.name + "\n");
      $("#user_history").append("Cognome: " + user.surname + "\n");
      $("#user_history").append("Username: " + user.username + "\n");
      $("#user_history").append("Password: " + user.password + "\n\n");
    }
  }

}

function show_users(result, id) {
  document.getElementById('list_users').textContent = "";
  output = JSON.parse(result)
  if (output.Ok) {
    for (i in output.Ok) {
      $("#list_users").append("Utente " + (parseInt(i) + 1) + "\n");
      $("#list_users").append("Nome: " + output.Ok[i].name + "\n");
      $("#list_users").append("Cognome: " + output.Ok[i].surname + "\n");
      $("#list_users").append("Username: " + output.Ok[i].username + "\n");
      $("#list_users").append("Password: " + output.Ok[i].password + "\n\n");
    }
  }
}

function show_sensors(result, id) {
  document.getElementById('list_sensors').textContent = "";
  output = JSON.parse(result)
  if (output.Ok) {
    console.log(output.Ok[0].Ok.result.Single.entry.App[1])
    for (i in output.Ok) {
      var sensor = JSON.parse(output.Ok[0].Ok.result.Single.entry.App[1]);
      $("#list_sensors").append("Sensore " + (parseInt(i) + 1) + "\n");
      $("#list_sensors").append("Nome: " + sensor.name + "\n");
      $("#list_sensors").append("Tipologia: " + sensor.sensor_type + "\n");
      $("#list_sensors").append("Quanto: " + sensor.quanto + "\n");
      $("#list_sensors").append("Tag_loc: " + sensor.tag_loc + "\n");
      $("#list_sensors").append("State: " + sensor.state + "\n");
      $("#list_sensors").append("Lat: " + sensor.lat + "\n");
      $("#list_sensors").append("long: " + sensor.long + "\n\n");
    }
  }
}

function show_user(result) {
  var name = document.getElementById('user_name');
  var surname = document.getElementById('user_surname');
  var username = document.getElementById('user_username');
  var psw = document.getElementById('user_psw');
  var output = JSON.parse(result);
  if (output.Ok) {
    name.textContent = output.Ok.name;
    surname.textContent = output.Ok.surname;
    username.textContent = output.Ok.username;
    psw.textContent = output.Ok.password;
  } else {
    alert(output.Err.Internal);
  }
}

function show_user_by_id(result) {
  var name = document.getElementById('user_name');
  var surname = document.getElementById('user_surname');
  var username = document.getElementById('user_username');
  var psw = document.getElementById('user_psw');
  var output = JSON.parse(result);
  if (output.Ok) {
    var user = JSON.parse(output.Ok[0].Ok.result.Single.entry.App[1]);
    name.textContent = user.name;
    surname.textContent = user.surname;
    username.textContent = user.username;
    psw.textContent = user.password;
  } else {
    alert(output.Err.Internal);
  }
}