
var holochain_connection = holochainclient.connect();

function register() {
    holochain_connection.then(({ callZome, close }) => {

        var name = $('#name').val();
        var surname = $('#surname').val();
        var username = $('#username').val();
        var psw = $('#psw').val();
        //const hash = digestMessage('SHA-256', psw).toString();
        //console.log(hash);
        callZome(
            'test-instance',
            'users',
            'create_user',
        )({
            user: {
                name: name,
                surname: surname,
                username: username,
                password: psw
            },
        }).then(result => {
            var user = JSON.parse(result);
            console.log(user);
            if (user.Ok) {
                console.log("User create");
                window.location.replace("./index.html");

            } else {
                alert("Error: user already exists!");
                location.reload();
            }
        }).catch(err => { alert("Error: user already exists!") }
        );
    });
}


// HASH NON UTILIZZATA A CAUSA DELLA GESTIONE ASINCRONA CHE DA PROBLEMI
// async function digestMessage(message) {
//     const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
//     const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
//     const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
//     const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
//     console.log("dentro== "+hashHex);
//     return hashHex;
//   }